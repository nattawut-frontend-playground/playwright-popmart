install
```bash
npm init playwright@latest
```

run
```bash
npx playwright test /popmart-official/popmart.spec.ts --headed  ;
```

record actions and convert to scripts
```bash
npx playwright codegen
```
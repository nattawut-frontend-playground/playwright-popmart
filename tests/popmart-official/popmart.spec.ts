import { test } from '@playwright/test';
import { chromium } from 'playwright';

// const url = 'https://www.popmart.com/th/products/888/crybaby-%C3%97-powerpuff-girls-series-vinyl-face-plush-blind-box';
const url = 'https://www.popmart.com/th/products/646/Minions-Travelogues-of-China-Series-Figures';
const userDataDir = '/Users/nattawutpanyakamonkit/Library/"Application Support"/Google/Chrome';



test('already signed in', async () => {
    test.setTimeout(100000); //manually set timeout for this test
    const browser = await chromium.launchPersistentContext(userDataDir, { headless: false, channel: 'chrome' });
    const page = await browser.newPage()

    await page.goto(url);
    await page.getByText('Whole Set', { exact: true }).click(); //choose set box

    // loop check until available
    // if count = 1, item's available. 
    // if count < 1, item's unavailable.
    var btn = await page.getByText('BUY NOW').count() 
    while (btn < 1) {
        if (btn == 0) {
            await page.reload();
            await page.getByText('Whole Set', { exact: true }).click();
            btn = await page.getByText('BUY NOW').count()
        }
    }
    console.log("now available to sale", btn)


    await page.getByText('Whole Set', { exact: true }).click();
    await page.getByText('+').click(); // add quantity





    await page.getByText('BUY NOW').click();
    await page.getByText('TrueMoney WalletAlipay+').click(); // choose payment type
    await page.waitForTimeout(1200); // the popmart web is slow so it need to delay to make transaction successsful
    await page.getByRole('button', { name: 'PLACE ORDER' }).click();


    await page.waitForTimeout(10000); 
});

// test('not sign in', async () => {
//     const browser = await chromium.launchPersistentContext('/Users/nattawutpanyakamonkit/Library/"Application Support"/Google/Chrome/playwright', { headless: false, channel: 'chrome' });
//     const page = await browser.newPage()

//     await page.goto(url);
//     await page.getByText('ACCEPT').click();
//     await page.getByText('BUY NOW').click();
//     await page.getByPlaceholder('Enter your e-mail address').click();
//     await page.getByPlaceholder('Enter your e-mail address').fill('xxx@gmail.com');
//     await page.getByRole('button', { name: 'CONTINUE' }).click();
//     await page.getByPlaceholder('Enter your password').click();
//     await page.getByPlaceholder('Enter your password').fill('xxx');
//     await page.getByPlaceholder('Enter your password').press('Enter');
//     await page.getByText('TrueMoney WalletAlipay+').click();
//     await page.waitForTimeout(1200);
//     await page.getByRole('button', { name: 'PLACE ORDER' }).click();

//     await page.waitForTimeout(1000000);
// });